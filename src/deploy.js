/**
 * Example Config:
 * ```
  {
    deploy:{
        entry:'./build/**\/*.*',
        base:'./build',
        output: '/path/on/ftp/server',
        ftp:{
            host: 'myserver.de',
            user: 'admin',
            password: '1337',
        }
    },
  }
 * ```
 */

const ftp = require('vinyl-ftp');
const {log,stopwatch,error} = require('./util').logger('deploy');
const {argv} = require('yargs');


module.exports = function deploy(gulp,config = {}){

    function upload(){
        const timer = stopwatch('starting...');
        const ftpConfig = Object.assign({parallel:10,log},config.deploy.ftp);
        const {entry,base,output} = config.deploy;
        const conn = ftp(ftpConfig);
        return gulp.src(entry, { base: base || '.', buffer: false })
            .on('end',()=>timer('deployed'))
            .pipe(conn.newer(output))
            .pipe(conn.dest(output));
    }

    if(argv.dirty) {
        gulp.task('deploy',upload);
    }else{
        gulp.task('deploy',['build'],upload);
    }

    return {};
}

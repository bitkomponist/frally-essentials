/**
 * Example Config:
 * ```
    {
      assets:{
        entry:'./assets/*.{png,gif,jpg,jpeg,svg}',
        watch:'./assets/*.{png,gif,jpg,jpeg,svg}',
        copy:'./assets/!(*.png|*.gif|*.jpg|*.jpeg|*.svg)',
        output:'./build'
      }
    }
 * ```
 */

const {log,info,stopwatch,error} = require('./util').logger('assets');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const copy = require('gulp-copy');

module.exports = function buildAssets(gulp,config = {}){
    let timer;
    gulp.task('copy-assets',
        ()=>{
          timer = stopwatch('starting...');
          return gulp.src(config.assets.copy)
              .pipe(newer(config.assets.output))
              .pipe(gulp.dest(config.assets.output));
        }
    );

    gulp.task('optimize-assets',()=>(
        gulp.src(config.assets.entry)
            .pipe(newer(config.assets.output))
            .pipe(imagemin(config.assets.plugins, config.assets.options))
            .pipe(gulp.dest(config.assets.output))
    ));

    gulp.task('build-assets',['copy-assets','optimize-assets'],(callback)=>{(timer||info)('build complete');callback();});

    gulp.task('watch-assets',['build-assets'],()=>gulp.watch(config.assets.watch,['build-assets']));

    return {
      build:{"build-assets":true},
      watch:{"watch-assets":true}
    };
}

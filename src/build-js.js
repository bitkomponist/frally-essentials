/**
 * Example config:
 *
 *
 * ```
 *  {
      js:{
          entry: {
              main:'./src/js/main.js',
              vendor:'./src/js/vendor.js'
          },
          output:{
              path: join(__dirname,"./build/js"),
              filename: "[name].js",
              publicPath: '/js/',
          },
          module: {
              loaders: [
                  {
                      test: /\.js$/,
                      exclude: /(node_modules|bower_components)/,
                      loader: 'babel-loader',
                      query: {
                          presets: ['latest']
                      }
                  }
              ]
          }
      },
      development:{
        js:{
            plugins:[
                new CommonsChunkPlugin('vendor'),
            ],
            devtool: "source-map",
            devServer: {
                publicPath:'/js/',
                contentBase: join(__dirname, "./build"),
                inline: true,
                port:8080,
                host:'localhost',
                open:true,
            },
        },
      },
      production:{
        js:{
              plugins:[
                  new CommonsChunkPlugin('vendor'),
                  new UglifyJsPlugin(),
              ],
        },
      }
 *  }
 * ```
 *
 */

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const chalk = require('chalk');
const {PluginError} = require('gulp-util');
const {log,error,info,warn} = require('./util').logger('js');
const extend = require('extend');

const DEV_SERVER_DEFAULTS = {
  // It suppress error shown in console, so it has to be set to false.
  quiet: false,
  // It suppress everything except error, so it has to be set to false as well
  // to see success build.
  noInfo: false,
  stats: {
    // Config for minimal console.log mess.
    assets: false,
    colors: true,
    version: false,
    hash: false,
    timings: false,
    chunks: false,
    chunkModules: false
  }
};

module.exports = function buildJS(gulp,config = {}){

    webpackConfig = Object.assign({},config.js);

    function getCompiler(callback,watch=false){
        const config = Object.assign({},webpackConfig,{watch});
        log('startup');
        return webpack(config,(e,stats)=>{
            if(e){
                throw new PluginError("js",e);
            }
            callback&&callback();
        });
    }

    gulp.task('build-js',getCompiler);

    const webpackWatch = () => {
      // gulp.watch(config.js.watchEntry, ['build-js']);
      // callback();
      const {compiler} = getCompiler(null,true);

      compiler.plugin('compile',(compilation)=>{
          log('compiling...')

      });

      compiler.plugin('done',({hash,startTime,endTime,compilation})=>{
        log('done');
        const {warnings,errors} = compilation;
        info(`build-time: ${Math.round((endTime - startTime) / 1000)}s, hash: ${hash}`);

        if(warnings){
            warnings.forEach(warn);
        }

        if(errors){
            errors.forEach((e)=>error(e,false));
        }

      });

      compiler.plugin('failed',error);
    };
    const webpackDevServer = (callback)=>{
        const devServer = new WebpackDevServer(getCompiler(),extend(true,{},DEV_SERVER_DEFAULTS,webpackConfig.devServer));
        const {port,host} = webpackConfig.devServer;
        devServer.listen(port||8080,host||'0.0.0.0',(e)=>{
            if(e){
                throw new PluginError("dev-server",e);
            }
            log(`http://${host}:${port}/webpack-dev-server/index.html`);
        });
    }

    gulp.task('watch-js',(callback)=>{
      if(!config.js.devServer){
        webpackWatch(callback);
      }else{
        webpackDevServer(callback);
      }
    });

    return {
        build:{"build-js":true},
        watch:{"watch-js":true}
    };
}

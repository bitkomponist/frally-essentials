/**
 * Example Config:
 *
 *
 * ```
 *  {
      css:{
          entry:'./src/scss/master.scss',
          watch:'./src/scss/*.scss',
          output:'./build/css',
          sass:{
              includePaths:[
                  join(__dirname,'node_modules'),
                  join(__dirname,'node_modules/bootstrap-sass/assets/stylesheets')
              ]
          }
      },
      production:{
        css:{
            postcss:{
                plugins:[autoprefixer({browsers:[
                    "> 1%",
                    "last 2 versions",
                    "IE 9",
                    "IE 10"
                ]}),cssnano()],
            }
        },
      },
 *   }
 * ```
 */
const {log,info,stopwatch,error} = require('./util').logger('css');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');

module.exports = function buildCSS(gulp,config = {}){
    gulp.task('build-css',()=>{
        const timer = stopwatch('starting...');
        const css = gulp.src(config.css.entry)
            .pipe(sass(config.css.sass || {}).on('error',function(e){
              error(e);
              this.emit('end');
            }));

        if(config.css.postcss && config.css.postcss.plugins){
            css.pipe(postcss(config.css.postcss.plugins));
        }

        css.on('end',()=>{timer('build complete');});

        return css.pipe(gulp.dest(config.css.output));
    });


    gulp.task('watch-css',['build-css'],()=>gulp.watch(config.css.watch,['build-css']));

    return {
      build:{"build-css":true},
      watch:{"watch-css":true}
    }
}

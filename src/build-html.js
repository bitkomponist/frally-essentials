/**
 * Example Config:
 * ```
    {
      html:{
        entry:'./src/pug/views/*.pug',
        watch:['./src/pug/*.{pug,js}',join(__dirname,'data/*.*')],
        output:'./build',
        options:{
            locals:join(__dirname,'./data/index.js'),
            helpers:join(__dirname,'./src/pug/helpers/index.js'),
      }
    }
 * ```
 */

const {PluginError} = require('gulp-util');
const {log,info,stopwatch,error} = require('./util').logger('html');
const {dirname} = require('path');
const pug = require('gulp-pug');
const pugInheritance = require('gulp-pug-inheritance');
const changed = require('gulp-changed');
const cached = require('gulp-cached');
const gulpif = require('gulp-if');
const filter = require('gulp-filter');
const debug = require('gulp-debug');


const extend = require('extend');
const decache = require('decache');

function bindHelpers(helpers,locals){
   Object.keys(helpers).forEach((key)=>{
       if(typeof helpers[key] === 'function'){
           locals[key] = helpers[key].bind(locals);
       }
   });
}

module.exports = function buildHTML(gulp,config = {}){
   function getOptions(){

       let locals = {config};
       let helpers = {};

       let {options} = config.html;

       if(!options) return {locals};

       // invalidate if locals is a module
       if(typeof options.locals === 'string'){
           decache(options.locals);
           extend(true,locals,require(options.locals));
       }else if(options.locals){
           extend(true,locals,options.locals);
       }

       // invalidate if locals is a module
       if(typeof options.helpers === 'string'){
           decache(options.helpers);
           extend(true,helpers,require(options.helpers));
       }else if(options.helpers){
           extend(true,helpers,options.helpers);
       }

       locals = extend(locals,{config});

       options = extend(true,{},options,{locals});

       bindHelpers(helpers,options.locals);

       return options;
   }

   let isWatching = false;

   gulp.task('build-html',()=>{
     const timer = stopwatch('starting...');
     return gulp.src(config.html.entry)
        // .pipe(gulpif(isWatching,cached('pug')))
        .pipe(pug(getOptions()).on('error',function(e){
           error(e);
           this.emit('end');
        }))
        .on('end',()=>timer('build complete'))
        .pipe(gulp.dest(config.html.output));
   });


   /*gulp.task('build-html',()=>{
     const filterFunction = "partialFilter" in config.html?(config.html.partialFilter):(file)=>(!/\/_/.test(file.path) && !/^_/.test(file.relative));
     const timer = stopwatch('starting...');

     return gulp.src(config.html.entry)
        .pipe(changed(config.html.output,{extension:'.html'}))
        .pipe(debug({title:'after-changed'}))
        .pipe(gulpif(isWatching,cached('pug')))

        .pipe(pugInheritance({basedir:config.html.basedir + '/views',skip:'node_modules'}))

        .pipe(filter(filterFunction))

        .pipe(pug(getOptions()).on('error',function(e){
            error(e);
            this.emit('end');
        }))
        .on('end',()=>timer('build complete'))
        .pipe(debug({title:'after-filter'}))
        .pipe(gulp.dest(config.html.output));
   });*/

   gulp.task('set-watching-html',()=>{
     isWatching = true;
   });

   //todo continue after error
   gulp.task('watch-html',['build-html','set-watching-html'],()=>gulp.watch(config.html.watch,['build-html']));

   return {
     build:{"build-html":true},
     watch:{"watch-html":true}
   };
}

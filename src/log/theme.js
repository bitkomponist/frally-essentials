const chalk = require('chalk');

const tint = {
  danger:(target)=>target.red,
  warning:(target)=>target.yellow,
  default:(target)=>target.gray,
  success:(target)=>target.green,
  info:(target)=>target.cyan
}

const label = tint.info(chalk.bold);
const zf = (s)=>((s+"").length<2?`0${s}`:s);
const date = ()=>{
  const d = new Date();
  return tint.default(chalk.dim)(`${zf(d.getHours())}:${zf(d.getMinutes())}:${zf(d.getSeconds())} `)
}
const text = (msg,_tint="default")=>tint[_tint](chalk.reset)(msg);

module.exports = {
  log(_label,msg){
    return date() + label(`[${_label}] `) + text(msg);
  },
  info(_label,msg){
    return date() + label(`[${_label}] `) + text(msg,"info");
  },
  success(_label,msg){
    return date() + label(`[${_label}] `) + text(msg,"success");
  },
  warn(_label,msg){
    return date() + label(`[${_label}] `) + text(msg,"warning");
  },
  error(_label,e){
    if(e instanceof Error){
      return date() + label(`[${_label}] `) + text(e.message,"danger");
    }else{
      return date() + label(`[${_label}] `) + text(e,"danger");
    }
  }
};

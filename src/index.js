module.exports = {
  buildAssets: require('./build-assets'),
  buildCss: require('./build-css'),
  buildHtml: require('./build-html'),
  buildJs: require('./build-js'),
  deploy: require('./deploy'),
};

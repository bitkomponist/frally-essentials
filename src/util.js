const theme = require('./log/theme');

function logger(label="global",_theme=theme){
  const t = (fn)=>{
    return (msg)=>{
      if(typeof msg === 'string'){
        console.log(_theme[fn](label,msg));
      }else{
        console.log(_theme[fn](label,''));
        console.log(msg);
      }
    }
  };
  const api = {
    log:t("log"),
    info:t("info"),
    stopwatch(msg){
      const t = new Date();
      api.info(msg);

      return (msg)=>{
        const tEnd = new Date();
        const s = tEnd.getTime() - t.getTime();
        api.info(`${msg} (in ${s}ms)`);
      }
    },
    success:t("success"),
    warn:t("warn"),
    error(e,stack=true){
      console.log(_theme.error(label,e));
      if(stack && e.stack){
        console.log(e.stack);
      }
    }
  };
  return api;
}
Object.assign(logger,logger());
module.exports = {
  logger,
};
